﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Media_Assignment1.Services
{
    public interface ILocalNotifications
    {
        void SendLocalNotification(string title, string description, int iconID);
    }
}
