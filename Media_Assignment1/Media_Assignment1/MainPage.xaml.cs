﻿using Plugin.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;    
using Xamarin.Forms;

namespace Media_Assignment1
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            
		}
        void PhotoPage(Object s,EventArgs e)
        {
            Navigation.PushAsync(new Views.Photo());
        }
        void ShareLink(Object s, EventArgs e)
        {
            Navigation.PushAsync(new Views.Share());
        }
        void ScanBarcode(Object s, EventArgs e)
        {
            Navigation.PushAsync(new Views.BarCode());
        }
        void Notification(Object s, EventArgs e)
        {
            Navigation.PushAsync(new Views.LocalNotification());
        }
        void GPS(Object s, EventArgs e)
        {
            Navigation.PushAsync(new Views.GPS());
        }
        void Maps(Object s, EventArgs e)
        {
            Navigation.PushAsync(new Views.Maps());
        }
        void Scratch(Object s,EventArgs e)
        {
            Navigation.PushAsync(new Views.ScratchPad());
        }
    }
}
