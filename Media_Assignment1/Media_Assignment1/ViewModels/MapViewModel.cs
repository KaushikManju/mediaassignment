﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using Media_Assignment1.Views;

namespace Media_Assignment1.ViewModel
{
    class MapViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public MapViewModel()
        {
            List<Pin> pins = new List<Pin>()
            {
                 new Pin() { Position = new Position(40.76448593664362, -73.97362968774416), Type = PinType.Generic, Label = "Trump Tower",Address="https://www.trump.com" },
                 new Pin() { Position = new Position(40.758895, -73.98513100000002), Type = PinType.Generic, Label = "Times Square",Address="https://en.wikipedia.org/wiki/Times_Square" },
                  new Pin() { Position = new Position(40.7484405, -73.98566440000002), Type = PinType.Generic, Label = "Empire State Building",Address="https://en.wikipedia.org/wiki/Empire_State_Building" }
            };
            //foreach (var p in pins)
            //{

            //    PinCollection.Add(p);
            //    //p.Clicked += P_Clicked;

            //}

            for (int i = 0; i < 3; i++)
            {
                var pin = new Pin();
                pin.Position = pins[i].Position;
                pin.Type = pins[i].Type;
                pin.Label = pins[i].Label;
                pin.Address = pins[i].Address;
                pin.Clicked += P_Clicked;
                PinCollection.Add(pin);
            }
            MyPosition = new Position(40.758895, -73.98513100000002);
        }

        private void P_Clicked(object sender, EventArgs e)
        {
            var loc = sender as Pin;
            ClinicsMaps cm = new ClinicsMaps();
            cm.Navigate(loc);
        }

        private void Pin_Clicked(object sender, EventArgs e)
    {

    }
    private ObservableCollection<Pin> _pinCollection = new ObservableCollection<Pin>();
        public ObservableCollection<Pin> PinCollection { get { return _pinCollection; } set { _pinCollection = value; OnPropertyChanged(); } }

        private Position _myPosition = new Position(-37.8141, 144.9633);
        public Position MyPosition { get { return _myPosition; } set { _myPosition = value; OnPropertyChanged(); } }
    }
}
