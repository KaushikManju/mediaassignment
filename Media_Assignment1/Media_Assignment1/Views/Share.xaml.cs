﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Permissions;
using Plugin.Share;
using Plugin.Share.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Media_Assignment1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Share : ContentPage
    {
        public Share()
        {
            InitializeComponent();
        }
        private void Share_text(object sender, EventArgs e)
        {

            CrossShare.Current.Share(new ShareMessage
            {
                Title = "Title",
                Text = "CheckOut new way",
                Url = "http://Google.com"
            });
        }
        private void Share_Link(object sender, EventArgs e)
        {
            CrossShare.Current.Share(new ShareMessage
            {

                Url = "http://Google.com"
            }, new ShareOptions
            {
                ChooserTitle = "Share Blog",
                ExcludedUIActivityTypes = new[] { ShareUIActivityType.PostToFacebook }
            });

        }
        private void Open_Browser(object sender, EventArgs e)
        {
            if (!CrossShare.IsSupported)
                return;
            CrossShare.Current.OpenBrowser("http://www.facebook.com");
        }
    }
}