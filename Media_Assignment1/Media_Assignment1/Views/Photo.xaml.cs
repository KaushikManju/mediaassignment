﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Media_Assignment1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Photo : ContentPage
	{
		public Photo ()
		{
			InitializeComponent ();
		}
        public async void TakePhoto_Button_Clicked(Object s, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                {
                    await DisplayAlert("No Camera", ":( No Camera Available", "ok");
                    return;
                }

                MediaFile file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    SaveToAlbum = true,
                    DefaultCamera = CameraDevice.Rear,
                    Name = "image.jpeg"
                });
                if (file == null)
                    return;
                PathLabel.Text = file.AlbumPath;
                MainImage.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async void PickPhoto_Button_Clicked(Object s, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsPickPhotoSupported)
                {
                    await DisplayAlert("Oops", ":( Pick Photo Not Supported", "ok");
                    return;
                }

                MediaFile file = await CrossMedia.Current.PickPhotoAsync();
                if (file == null)
                    return;
                PathLabel.Text = "Photo Path : " + file.AlbumPath;
                MainImage.Source = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    file.Dispose();
                    return stream;
                });

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async void TakeVideo_Button_Clicked(Object s, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
                {
                    await DisplayAlert("No Camera", ":( No Camera Available", "ok");
                    return;
                }

                MediaFile file = await CrossMedia.Current.TakeVideoAsync(new StoreVideoOptions
                {
                    SaveToAlbum = true,
                    Quality = VideoQuality.High,
                    Name="video1.mp4"
                });
                if (file == null)
                    return;
                PathLabel.Text = file.AlbumPath;
                MainVideo.Source = file.AlbumPath;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public async void PickVideo_Button_Clicked(Object s, EventArgs e)
        {
            try
            {
                await CrossMedia.Current.Initialize();

                if (!CrossMedia.Current.IsPickVideoSupported)
                {
                    await DisplayAlert("Oops", ":( Pick Photo Not Supported", "ok");
                    return;
                }

                MediaFile file = await CrossMedia.Current.PickVideoAsync();
                if (file == null)
                    return;
                PathLabel.Text = "Photo Path : " + file.AlbumPath;
                MainVideo.Source = file.AlbumPath;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}