﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;
using Media_Assignment1.ViewModel;

namespace Media_Assignment1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Maps : ContentPage
    {
        Pin pinNew = new Pin();
        public Maps()
        {
            InitializeComponent();
            BindingContext = new MapViewModel();
        }
        //    GetLocation();
        //}
        //public void LoadMap(Plugin.Geolocator.Abstractions.Position p)
        //{
        //    var map = new Map(MapSpan.FromCenterAndRadius(
        //      new Xamarin.Forms.Maps.Position(40.758895, -73.98513100000002),
        //      Distance.FromMiles(0.5)))
        //    {
        //        IsShowingUser = true,
        //        VerticalOptions = LayoutOptions.FillAndExpand

        //    };
        //    var position1 = new Xamarin.Forms.Maps.Position(40.758895, -73.98513100000002);
        //    var pin = new Pin
        //    {
        //        Type = PinType.Place,
        //        Position = position1,
        //        Label = "Times Square",
        //        Address = "www.inherit.com"
        //    };
        //    pinNew = pin;
        //    map.Pins.Add(pin);
        //    var position2 = new Xamarin.Forms.Maps.Position(40.76448593664362, -73.97362968774416);
        //    var pin1 = new Pin
        //    {
        //        Type = PinType.Place,
        //        Position = position2,
        //        Label = "Trump Tower",
        //        Address = "www.inherit.com"
        //    };
        //    map.Pins.Add(pin1);

        //    var position3 = new Xamarin.Forms.Maps.Position(40.7484405, -73.98566440000002);
        //    var pin3 = new Pin
        //    {
        //        Type = PinType.Place,
        //        Position = position3,
        //        Label = "Empire State Building",
        //        Address = "www.inherit.com"
        //    };
        //    map.Pins.Add(pin3);
        //    pin.Clicked += Pin_Clicked;
        //    Content = map;
        //}

        //private void Pin_Clicked(object sender, EventArgs e)
        //{
        //    Navigation.PushPopupAsync(new ClinicDetails(pinNew));
        //}

        //public async void GetLocation()
        //{
        //    Plugin.Geolocator.Abstractions.Position position = null;
        //    try
        //    {
        //        var locator = CrossGeolocator.Current;
        //        locator.DesiredAccuracy = 50;
        //        if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
        //        {
        //            await DisplayAlert("Maps Error", "Please Enable the GPS On Your Device!!!", "OK");
        //        }

        //        position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
        //    }
        //    catch (Exception ex)
        //    {
        //        await DisplayAlert("Maps Error", "The GPS is Not able to detect the location", "Close");
        //    }
        //    LoadMap(position);
        //}
    }
}
