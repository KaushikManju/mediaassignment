﻿using Media_Assignment1.Models;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Media_Assignment1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ImageCrop : PopupPage
	{
		public ImageCrop ()
		{
			InitializeComponent ();
            getImage();
        }

        private async void getImage()
        {

            var page = new ScratchPad();
            DbConnection dbConnection = new DbConnection();
            var sign = await dbConnection.ReturnSign();
            cropImage.Source = ImageSource.FromStream(() => new MemoryStream(sign[0].Signature));
            //  Sign.Source = ImageSource.FromStream();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
           var image= await cropImage.GetImageAsJpegAsync();
            cropImage.IsVisible = false;
            
            cachedimage.Source = ImageSource.FromStream(() => image);
        }
    }
}