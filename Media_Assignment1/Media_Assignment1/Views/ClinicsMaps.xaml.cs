﻿using Media_Assignment1.ViewModel;
using Plugin.Geolocator;
using Rg.Plugins.Popup.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Media_Assignment1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ClinicsMaps : ContentPage
	{
		public ClinicsMaps ()
		{
			InitializeComponent ();
            BindingContext = new MapViewModel();

        }
        public async void Navigate(Pin p)
        {
          await  Navigation.PushPopupAsync(new ClinicDetails(p));
        }
     
    }
}