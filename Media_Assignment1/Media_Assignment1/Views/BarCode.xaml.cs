﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ZXing.Net.Mobile.Forms;

namespace Media_Assignment1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BarCode : ContentPage
	{
		public BarCode ()
		{
			InitializeComponent ();
		}
        private void Scan_clicked(object sender,EventArgs e )
        {
            var code = new ZXingScannerPage();
            Navigation.PushModalAsync(code);
            code.OnScanResult += (result) =>
              {
                  Device.BeginInvokeOnMainThread(async () =>
                  {
                      await Navigation.PopModalAsync();
                      lblScannedCode.Text = result.Text;
                  });
              };
        }
        
    }
}