﻿using Media_Assignment1.Models;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace Media_Assignment1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClinicDetails : PopupPage
    {
        MapDetails m = new MapDetails();
        public ClinicDetails(Pin pin)
        { 
            InitializeComponent();
          
            m.ImageUrl = "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Trump_Tower_%287181836700%29_%28cropped%29.jpg/800px-Trump_Tower_%287181836700%29_%28cropped%29.jpg";
            m.Name = pin.Label;
            m.Address = pin.Address;
            BindingContext = m;
        }

        private void Set_Clicked(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(m.Address));
        }

    }
    
}