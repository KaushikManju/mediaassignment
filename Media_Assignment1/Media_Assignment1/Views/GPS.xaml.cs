﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace Media_Assignment1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GPS : ContentPage
    {
        public GPS()
        {
            InitializeComponent();
        }

        private async void Button_CLicked(object sender,EventArgs e)
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    //got a cahched position, so let's use it.
                    lblLastLatitude.Text = position.Latitude.ToString();
                    lblLastLongitude.Text = position.Longitude.ToString();
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    lblError.Text = "Please Enable the GPS On Your Device!!!";
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);
                lblCurrentLatitude.Text = position.Latitude.ToString();
                lblCurrentLongitude.Text = position.Longitude.ToString();


            }
            catch (Exception ex)
            {
                lblError.Text = "The GPS is Not able to detect the location";
            }

            if (position == null)
                lblError.Text = "The GPS is Not able to detect the location";
        }
        private void getWeather(object sender, EventArgs e)
        {
            //string cityName = city.Text;
            string url = "https://api.openweathermap.org/data/2.5/weather?lat=" + lblCurrentLatitude.Text + "&lon=" + lblCurrentLongitude.Text +
                        "&APPID=6363e519450c6d96f8d9c097959651ca";
            FetchWeatherAsync(url);
            //JsonValue json= await FetchWeatherAsync(url);
            //ParseAndDisplay(json);
        }
        private async void FetchWeatherAsync(string url)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";
            JsonValue jsonDoc;
            using (WebResponse response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    //Console.Out.WriteLine("Response:{0}", jsonDoc.ToString());
                    //return jsonDoc;

                }
            }

            try
            {
                //var text = JsonObject.Parse(jsonDoc.ToString());
                //string weather = text.ToString();

                var main = jsonDoc["main"];
                double maxtemp = main["temp_max"];
                double degreeCelsius = maxtemp - 273;
                //double degreeCelsius=(maxtemp-32)*(temp);
                lblWeather.Text = "The Temparature in Degree Celsius is " + degreeCelsius.ToString();
            }
            catch (Exception ex)
            {
                lblWeather.Text = ex.ToString();
            }
        }
    }
}