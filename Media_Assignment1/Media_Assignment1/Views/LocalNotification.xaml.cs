﻿using Plugin.LocalNotifications;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Media_Assignment1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LocalNotification : ContentPage
	{
		public LocalNotification ()
		{
			InitializeComponent ();
          

        }
        private void notify(object sender,EventArgs e)
        {

            CrossLocalNotifications.Current.Show("Media Assignment 1", "Test Notification");

        }
    }
}