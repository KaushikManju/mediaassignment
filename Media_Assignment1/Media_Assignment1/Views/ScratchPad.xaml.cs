﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using Media_Assignment1.Models;
using Media_Assignment1.Services;
using Plugin.Screenshot;
using Rg.Plugins.Popup.Extensions;
namespace Media_Assignment1.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScratchPad : ContentPage
	{
        SQLiteAsyncConnection db;
		public ScratchPad ()
		{
            
			InitializeComponent ();
            var databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Signature.db");
            db = new SQLiteAsyncConnection(databasePath);
            db.CreateTableAsync<Sign>();
		}

        private async void Save(object sender, EventArgs e)
        {
           // buttons.IsVisible = false;
            Sign = await scratch.GetImageStreamAsync(SignaturePad.Forms.SignatureImageFormat.Jpeg);
            var stream = new MemoryStream(await CrossScreenshot.Current.CaptureAsync());
            Rectangle crop = new Rectangle();

            //Bitmap
            //Image images = new Image();
            var SaveImage = new MemoryStream();
            stream.CopyTo(SaveImage);
            var SaveImageByteArray = SaveImage.ToArray();
             SignVIew.Source = ImageSource.FromStream(() =>(stream));
            //string ImageToGallery = Convert.ToBase64String(SaveImageByteArray);
            //byte[] ImageINBytes = Convert.FromBase64String(ImageToGallery);
            DependencyService.Get<ISign>().SavePictureToDisk("image", SaveImageByteArray);
            //StoreCameraMediaOptions storeMediaOptions = new StoreCameraMediaOptions
            //{  SaveToAlbum=true,
            //  Name="Sign.jpeg"
            //};
            Sign sign = new Sign();
            sign.Signature = SaveImageByteArray;
            DbConnection db = new DbConnection();

            int x= await db.SaveImage(sign);
        }
        public Stream Sign { get; private set; } = null;

        private void Clear(object sender, EventArgs e)
        {
            Navigation.PushPopupAsync(new ImageCrop());
        }
     
    }
}