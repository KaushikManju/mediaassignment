﻿using Media_Assignment1.Models;
using Media_Assignment1.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Media_Assignment1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignImage : ContentPage
    {
        public SignImage()
        {
            InitializeComponent();
            getImage();

        }

        private async void getImage()
        {

            var page = new ScratchPad();
            DbConnection dbConnection = new DbConnection();
            var sign =await dbConnection.ReturnSign();
            Sign.Source = ImageSource.FromStream(() => new MemoryStream(sign[0].Signature));
            //  Sign.Source = ImageSource.FromStream();
        }
    }
}