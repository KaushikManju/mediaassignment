﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using SQLite;
namespace Media_Assignment1.Models
{
    public class Sign
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public byte[] Signature { get; set; }
     
    }
    public class DbConnection
    {
        string databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Signature.db");

        public async Task<int> SaveImage(Sign sign)
        { //SQLiteAsyncConnection db = new SQLiteAsyncConnection(databasePath);

            var db = new SQLiteAsyncConnection(databasePath);
            await db.CreateTableAsync<Sign>();
            await  db.QueryAsync<Sign>("Delete From Sign ");
            int x = await db.InsertAsync(sign);
            return x;
        }

        public async Task<List<Sign>> ReturnSign()
        {
            var db = new SQLiteAsyncConnection(databasePath);
            var r = await db.QueryAsync<Sign>("Select * From Sign");
            return r;
        }

    }
}
