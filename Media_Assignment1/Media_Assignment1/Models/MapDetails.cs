﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Media_Assignment1.Models
{
    public class MapDetails
    {
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
