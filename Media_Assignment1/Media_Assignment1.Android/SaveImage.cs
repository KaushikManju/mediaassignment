﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Media_Assignment1.Droid;
using Media_Assignment1.Services;
[assembly: Xamarin.Forms.Dependency(typeof(SaveImageAndroid))]
namespace Media_Assignment1.Droid
{
   public class SaveImageAndroid : ISign
    {
        public void SavePictureToDisk(string filename, byte[] imageData)
        {
             var dir = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDcim);
            var pictures = dir.AbsolutePath;
            string name = filename + DateTime.Now.ToString("yyyyMMddHHmmssfff")+".jpg";
            string filePath = System.IO.Path.Combine(pictures, name);
            try
            {
                System.IO.File.WriteAllBytes(filePath, imageData);
            }
            catch (Exception ex)
            {

                //throw;
            }
        }
    }
}