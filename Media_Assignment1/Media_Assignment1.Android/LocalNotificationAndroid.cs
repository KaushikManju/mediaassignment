﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Media_Assignment1.Droid;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Media_Assignment1.Services;

namespace Media_Assignment1.Droid
{
    public class LocalNotificationAndroid : ILocalNotifications
    {
        public LocalNotificationAndroid()
        {

        }

        public void SendLocalNotification(string title, string description, int iconID)
        {
            // throw new NotImplementedException();
            Notification.Builder builder = new Notification.Builder(Application.Context).
                SetContentTitle(title).
                SetContentText(description).
                SetSmallIcon(Resource.Drawable.icon);
            Notification notification = builder.Build();
            NotificationManager notificationManager = 
                Application.Context.GetSystemService(Context.NotificationService) as NotificationManager;

            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);


        }
    }
}