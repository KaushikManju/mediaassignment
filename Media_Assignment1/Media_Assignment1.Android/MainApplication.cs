﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Plugin.CurrentActivity;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Media_Assignment1.Droid
{
    [Application]
 public   class MainApplication:Application,Application.IActivityLifecycleCallbacks
    {
        public MainApplication(IntPtr handle,JniHandleOwnership transer):base(handle,transer)
        {

        }

        public void OnActivityCreated(Activity activity, Bundle savedInstanceState)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityDestroyed(Activity activity)
        {
           // throw new NotImplementedException();
        }

        public void OnActivityPaused(Activity activity)
        {
           // throw new NotImplementedException();
        }

        public void OnActivityResumed(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivitySaveInstanceState(Activity activity, Bundle outState)
        {
           // throw new NotImplementedException();
        }

        public void OnActivityStarted(Activity activity)
        {
            CrossCurrentActivity.Current.Activity = activity;
        }

        public void OnActivityStopped(Activity activity)
        {
            //throw new NotImplementedException();
        }

        public override void OnCreate()
        {
            base.OnCreate();
           // CrossCurrentActivity.Current.Init(this);
            RegisterActivityLifecycleCallbacks(this);
        }
        public override void OnTerminate()
        {
            base.OnTerminate();
            UnregisterActivityLifecycleCallbacks(this);
        }
    }
}